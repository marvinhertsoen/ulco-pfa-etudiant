
-- TODO List
data List a = Empty | Cons a (List a) deriving Show


-- sumList 
sumList :: Num a => List a -> a
sumList Empty = 0
sumList (Cons h t) = h + sumList t

-- concatList 
concatList :: List String -> String
concatList Empty = ""
concatList (Cons h t) = h ++ concatList t

-- flatList 

-- toHaskell 
toHaskell :: List a -> [a]
toHaskell Empty = []
toHaskell (Cons h t) = h:toHaskell t

-- fromHaskell 
fromHaskell :: [a] -> List a 
fromHaskell [] = Empty
fromHaskell (h:t) = Cons h (fromHaskell t)

-- myShowList
myShowList :: Show a => List a -> String
myShowList Empty = ""
myShowList (Cons h t) = show h ++ "," ++ myShowList t

main :: IO ()
main = do
    print $ (sumList (Cons 1 (Cons 2 Empty)))
    print $ (sumList (Cons 1 (Cons 2 Empty))::Int)
    print $ (concatList (Cons "Bonjour" (Cons " Marvin !" Empty))::String)
    print $ (toHaskell (Cons 3 (Cons 4 Empty)))
    print $ fromHaskell [3,4]
    print $ myShowList (Cons 3 (Cons 4 Empty))
