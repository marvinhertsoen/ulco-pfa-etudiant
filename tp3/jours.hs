
-- TODO Jour
data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche


-- estWeekend :: Jour -> Bool
estWeekend :: Jour -> Bool
estWeekend Samedi = True
estWeekend Dimanche = True
estWeekend _ = False

-- compterOuvrables :: [Jour] -> Int
compterOuvrables :: [Jour] -> Int
compterOuvrables [] = 0
compterOuvrables (h:t) = i + compterOuvrables t
    where i = if estWeekend h then 0 else 1



main :: IO ()
main = do
    print $ estWeekend Mardi
    print $ estWeekend Dimanche
    print $ compterOuvrables [Lundi, Mardi, Mercredi, Jeudi, Samedi, Dimanche]

