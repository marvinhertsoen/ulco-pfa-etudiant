
-- TODO User
data User = User 
    { _nom :: String,
      _prenom :: String,
      _age :: Int
    } 

-- showUser :: User -> String
showUser :: User -> String 
showUser user = "nom: " ++ _nom user ++ " prénom: " ++ _prenom user ++ " age:" ++  show (_age user) 

-- incAge :: User -> User
incAge :: User -> User
incAge user = user {_age = (_age user + 1)} 

main :: IO ()
main = do
    print $ showUser (User "Marvin" "HERTSOEN" 24)
    print $ showUser (incAge (User "Marvin" "HERTSOEN" 24))
