import Control.Monad (forM_)
import System.IO (hFlush, stdout)
import Text.Read (readMaybe)

--askNumber :: String 
--askNumber = getLine
saisirEntier :: Int -> IO()
saisirEntier i = do
    putStrLn $ "saisie " ++ show i ++ ":"
    -- extract string from IO String
    line <- getLine
    
    let mx = readMaybe line :: Maybe Int
    case mx of 
        Nothing -> putStrLn "Saisie invalide"
        Just x  -> putStrLn $ "Vous avez saisi l'entier: " ++ show x


    

main :: IO ()
main = do
    mapM_ saisirEntier [1..4]