
fizzbuzz1 :: [Int] -> [String]
fizzbuzz1 [] = []
fizzbuzz1 (h:t) = str : fizzbuzz1 t
    where str3 = if h `mod` 3 == 0 then "fizz" else "" 
          str5 = if h `mod` 5 == 0 then "buzz" else ""
          str35 = str3 ++ str5
          str = if str35 /= "" then str35 else show h
        
fizzbuzz2 :: [Int] -> [String]
fizzbuzz2 xs = aux xs []
    where aux [] acc = acc
          aux (x:xs) acc = aux xs (acc ++ [str])
            where str3 = if x `mod` 3 == 0 then "fizz" else "" 
                  str5 = if x `mod` 5 == 0 then "buzz" else ""
                  str35 = str3 ++ str5
                  str = if str35 /= "" then str35 else show x


fizzbuzz :: [String]
fizzbuzz = [show x | x <-[1..], mod x 3 == 0]

main :: IO ()
main = do
    print $ fizzbuzz1 [1..12]
    print $ fizzbuzz2 [1..12]