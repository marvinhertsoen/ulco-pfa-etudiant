--Écrivez un programme text3.hs équivalent mais qui lit un ByteString et convertit en Text.

import qualified Data.Text.IO as TSIO
import qualified Data.Text.Lazy.IO as TLIO
import qualified Data.Text.Lazy as TL


main :: IO()
main = do
    content <- TSIO.readFile "text1.hs"
    TLIO.putStrLn $ TL.fromStrict content


