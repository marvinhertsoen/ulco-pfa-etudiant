
{-# LANGUAGE OverloadedStrings #-}
import Data.Text as T

newtype Person = Person Text deriving Show

persons :: [Person]
--persons = [Person (T.pack "Marvin"), Person (T.pack "HERTSOEN")]
persons = [Person "Marvin", Person "HERTSOEN"]
main :: IO ()
main = print persons

