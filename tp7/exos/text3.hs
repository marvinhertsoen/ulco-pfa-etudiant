--Écrivez un programme text3.hs équivalent mais qui lit un ByteString et convertit en Text.

import qualified Data.Text.IO as TIO
import qualified Data.Text.Encoding as TE
import Data.ByteString.Char8 as B

main :: IO()
main = do
    content <- B.readFile "text1.hs"
    TIO.putStrLn $ TE.decodeUtf8 content


