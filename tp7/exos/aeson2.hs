{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Aeson
import qualified Data.Text as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Show)

instance FromJSON Person where
    parseJSON = withObject "Person" $ \v -> Person
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> v .: "birthyear"
        <*> v .: "speakenglish"

main :: IO ()
main = do
    --let res0 = Person "John" "Doe" "1970" False
    res1 <- eitherDecodeFileStrict "aeson-test1.json"
    print (res1 :: Either String Person)
    res2 <- eitherDecodeFileStrict "aeson-test2.json"
    print (res2 :: Either String [Person])
    res3 <- eitherDecodeFileStrict "aeson-test3.json"
    print (res3 :: Either String [Person])
    --print res0

