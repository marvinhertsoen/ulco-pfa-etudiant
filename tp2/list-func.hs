
-- mymap1 

mymap1 :: (a -> b) -> [a] -> [b]
mymap1 _ [] = []
mymap1 f (x:xs) = (f x) : mymap1 f xs

-- mymap2

-- myfilter1 
myfilter1 :: (a -> Bool) -> [a] -> [a]
myfilter1 _ [] = []
-- p = predicat
myfilter1 p (x:xs) = if p x then x:q else q
    where q = myfilter1 p xs
    
-- myfilter2 

-- myfoldl 

-- myfoldr 

main :: IO ()
main = do
    print $ mymap1 (*2) [1,2]
    print $ myfilter1 (<8) [1..20]

