
-- safeTailString 
safeTailString :: String -> String
safeTailString [] = "" 
safeTailString (h:t) = t 

-- safeHeadString 
safeHeadString :: String -> String
safeHeadString [] = ""
safeHeadString (h:t) = [h]
--correction
safeHeadString' :: String -> Maybe Char
safeHeadString' "" = Nothing
safeHeadString' (x:_) = Just x

-- safeTail 
safeTail :: [a] -> [a]
safeTail [] = []
safeTail (_:xs) = xs

-- safeHead 
safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:_) = Just x

main :: IO ()
main = do
    print $ safeTailString "Bonjour"
    print $ safeHeadString' "Bonjour"
    print $ safeTailString ""
    print $ safeHeadString ""
