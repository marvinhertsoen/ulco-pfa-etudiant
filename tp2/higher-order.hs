import Data.List

-- threshList
threshList :: Ord a => a -> [a] -> [a]
threshList _ [] = []
threshList max (h:t) = current:threshList max t
    where current =  if h < max then h else max

threshList' s xs = map (min s) xs

-- map : fait un calcul sur chaque élément d'une liste
-- filter : selectionne 
-- fold :: fait un calcul sur une liste et renvoie un élément

-- selectList
selectList :: Ord a => a -> [a] -> [a]
selectList s = filter (<s) 

-- maxList
maxList  :: Ord a => [a] -> a
maxList = foldr1 max 

main :: IO ()
main = do
    print $ threshList 3 [1..5::Int]

