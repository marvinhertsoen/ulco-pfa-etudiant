import Data.Char

-- isSorted
-- is sorted est de type [a] vers bool (où a est de type Ord (< > ...))
isSorted :: Ord a => [a] -> Bool
isSorted [] = True
isSorted [_] = True
isSorted (x0:x1:xs) = x0 < x1 && isSorted (x1:xs) 

-- nocaseCmp 
nocaseCmp :: String -> String -> Bool
nocaseCmp xs ys = (map toUpper xs) < (map toUpper ys) 
--nocaseCmp "" "" = True 


main :: IO ()
main = do
    --print $ isSorted []
    print $ isSorted [1]
    print $ isSorted [1,2,3,4]
    print $ isSorted [4,8,7,3]
    print $ nocaseCmp "Foo" "bar"
    print $ nocaseCmp "Foo" "foo"