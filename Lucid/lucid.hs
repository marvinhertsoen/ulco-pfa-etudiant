{-# LANGUAGE OverloadedStrings #-}

import Data.Text.Lazy as T
import Data.Text.Lazy.IO as T
import Lucid

main :: IO()
main = renderToFile "Lucid.html" $ do
    doctype_
    h1_ "hello" 
    p_ "world"