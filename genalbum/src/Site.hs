module Site where

import Data.Aeson
import GHC.Generics
import qualified  Data.Text.Lazy as T

data Site = Site
    {img :: [String]
    ,url :: String 
    } deriving (Generic, Show)

instance FromJSON Site

main :: IO ()
main = do
    --let res0 = Person "John" "Doe" "1970" False
    res1 <- eitherDecodeFileStrict "data/genalbum.json"
    print (res1 :: Either String Site)
