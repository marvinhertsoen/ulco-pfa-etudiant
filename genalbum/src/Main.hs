import Site
import Data.Aeson

main :: IO()
main = do 
    mSites <- decodeFileStrict "data/genalbum.json"
    case mSites of 
        Nothing -> putStrLn "loading failed"
        Just sites -> mapM_ print (sites :: [Site])